/* eslint-disable */
import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { changeRole, givePermissions } from '../actions';
import { RefreshConfig } from '../../../lib/react-gate';
import NotFound from './NotFound';

const mapDispatchToProps = dispatch => ({
  roleChanger: role => dispatch(changeRole(role)),
  add404: () => dispatch(RefreshConfig({ Component404: NotFound })),
  addPermissions: () => dispatch(givePermissions()),
});

const mapStateToProps = state => ({
  currentRole: state.user.role,
});

const Home = ({
  currentRole,
  roleChanger,
  add404,
  addPermissions,
}) => (
  <div className="container mt-5 text-center">
    <h1>React Gate Demo</h1>
    <h2 className="h3 mt-5">Test Pages</h2>
    <div className="mt-3">
      <Link to="/auth" className="btn btn-outline-dark">Auth route</Link>
      <Link to="/roleauth" className="btn btn-outline-dark mx-3">Role auth route</Link>
      <Link to="/permissionsauth" className="btn btn-outline-dark">Permissions protected route</Link>
    </div>

    <h2 className="h3 mt-5">Settings</h2>
    <div className="mt-3">
      <button className="btn btn-outline-dark" onClick={() => roleChanger(currentRole === 'admin' ? 'basic':'admin')} >Change role</button>
      <button className="btn btn-outline-dark mx-3" onClick={() => add404()}>Show 404</button>
      <button className="btn btn-outline-dark" onClick={() => addPermissions()}>Give user the permissions</button>
    </div>
  </div>
);

Home.propTypes = {
  currentRole: PropTypes.string.isRequired,
  roleChanger: PropTypes.func.isRequired,
  add404: PropTypes.func.isRequired,
  addPermissions: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
